0. add @billae et @guillaume.peillex for email attachment
1. interpret .md to report bold and underline text
2. put status of milestone (section title) in green (tex_gitlab_info.py:84)
3. tri table of closed
         -> a by project
         -> b by priority
         -> c by date
4. tri table of assigned opened
         -> a by project
         -> b by priority
         -> c by issue id
5. tri table of unassigned opened
         -> a by project
         -> b by priority
         -> c by issue id
6. algo still too slow (cf https://pycodequ.al/docs/pylint-messages/r0912-too-many-branches.html)
7. compile 2 times to get toc pdf
