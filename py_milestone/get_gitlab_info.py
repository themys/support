#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
module python
"""
import sys
import gitlab

class Getgitlabinfo():
    """
    Classe de definition des fonctions
    communiquant avec l api de gitlab pour
    rechercher des infos
    """

    def __init__(self, g_id, url, token):
        self.url = url
        self.token = token
        self.group_id = g_id

        # get connexion
        if not url:
            print("ERROR, url or token empty values", file=sys.stderr)
            sys.exit(1)

        try:
            if not token:
                gl_connect = gitlab.Gitlab(url=url)
            else:
                gl_connect = gitlab.Gitlab(url=url, private_token=token)


        except IOError as error:
            print("ERROR, can not connect to gitlab with this url/token "+error, file=sys.stderr)
            sys.exit(1)

        self.gitlab = gl_connect


        # get group
        try:
            self.group = self.gitlab.groups.get(int(self.group_id))
        except IOError as error:
            print("ERROR,"+error, file=sys.stderr)
            print("ERROR, This group id is not found : ", self.group_id, file=sys.stderr)
            sys.exit(1)

        self.group = self.gitlab.groups.get(self.group_id)
        self.projects = self.group.projects.list()





    def milestones(self):
        """
        Communication entre gitlab et des milestones de group
        """
        all_milestones = []

        if not self.token:
            print('You have to put a token', file=sys.stderr)
            print('See https://gitlab.com/gitlab-org/gitlab-foss/-/issues/64269', file=sys.stderr)


        try:
            self.group.milestones.list(get_all=True)

        except IOError as error:
            print("ERROR,"+error, file=sys.stderr)
            print('ERROR, Can not get milestones list.', file=sys.stderr)
            sys.exit(1)

        all_milestones = self.group.milestones.list(all=True)

        return sorted(all_milestones, key=lambda all_milestones: all_milestones.title)



    def issues(self):
        """
        Communication entre gitlab et des issues d'un groupe
        """
        return self.group.issues.list(all=True)

    def merge_requests(self):
        """
        Communication entre gitlab et des MR d'un groupe
        """
        return self.group.merge_requests(all=True)
