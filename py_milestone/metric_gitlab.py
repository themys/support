#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
module python internal
and this script
"""
import sys
import signal
import argparse

import get_gitlab_info
import tex_gitlab_info


def write_doc_by_milestones(group, url, token):
    """
    write pdf file on milestones order informations
    """
    # on ouvre la connection
    git = get_gitlab_info.Getgitlabinfo(group, url, token)
    group = git.group.name
    projects = git.projects

    # infos
    issues = git.issues()
    milestones = git.milestones()

    # pylatex
    tex_file = tex_gitlab_info.Texgitlabinfo(group)

    # entete
    tex_file.entete(group)

    # intro
    tex_file.introduction()


    # main loop
    for milestone in milestones:
        tex_file.section_entete(milestone)

        # issues
        if tex_file.check_issues(milestone):
            tex_file.tab_issues_closed(milestone, projects)         # closed during month -1
            tex_file.tab_issues_opened_attr(milestone, projects)    # opened attributed
            tex_file.tab_issues_opened_nonattr(milestone, projects) # opened unattributed

        # MR
        tex_file.section_tab_mr(milestone)                          # month -1

        tex_file.vspace(2.0)

    # issues opened without milestone
    tex_file.extra_section(issues, projects)

    # generate pdf
    tex_file.finish()


    print("1. Get info from group =", group)
    print("\t projects            =", len(projects))
    print("\t issues              =", len(issues))
    print("2. Pdf file is generated.")
    print("\t file                =", tex_file.fichier)


def usage():
    """
    Affiche l aide
    """
    print("run this script on python3 with arg : ")
    print(" ")
    print("  First put       <gitlab group id>   no default")
    print("  Second put      <personal token>    no default.")
    print(" ")


def signal_handler():
    """
    Pour interrompre le process lorsque l'on saisit "Ctrl+C"
    """
    print('Vous avez arrete le programme.')
    sys.exit(0)


if __name__ == "__main__":

    # ctrl + c
    # --------
    signal.signal(signal.SIGINT, signal_handler)

    # arguments
    # ---------
    PARSER_CONSTR = argparse.ArgumentParser(prog="Metric on GitLab",
                                            description="Scripting to get gitlab"+
                                            " milestone on group info and write pdf")
    PARSER_CONSTR.add_argument("group", help="Identifiant du groupe", type=int)
    PARSER_CONSTR.add_argument("token", help="Token", type=str)

    ARGS_CONSTR = PARSER_CONSTR.parse_args()

    if len(vars(ARGS_CONSTR)) != 2:
        print("ERROR : 2 necessary args", file=sys.stderr)
        usage()
        sys.exit(1)

    URL = "https://gitlab.com"
    GROUP = ARGS_CONSTR.group
    TOKEN = ARGS_CONSTR.token


    # write doc order by milestones
    # -----------------------------
    write_doc_by_milestones(GROUP, URL, TOKEN)
