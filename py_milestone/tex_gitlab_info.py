#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
module python internal
and pylatex
"""
import os
from datetime import date, timedelta
from pylatex import Document
from pylatex.utils import NoEscape
from pylatex.section import Section
from pylatex.base_classes import Command
from pylatex.table import Tabular
import pylatex.config as cf



class Texgitlabinfo():
    """
    Classe de definition des fonctions
    ecrivant un fichier latex a partir d'info gitlab
    """

    def __init__(self, group):

        cf.active = cf.Version1()
        self.doc = Document(geometry_options={"vmargin":"1.5cm", "lmargin":"1.5cm"})

        # le mois dernier
        lastday_lastmonth = date.today().replace(day=1) - timedelta(days=1)
        self.last_month = str(lastday_lastmonth)[:-3]

        # nom du fichier
        self.fichier = "pdl_" + str(group) + "_" + str(self.last_month)


#
#  ENTETE
#

    def entete(self, group):
        """
        premiere page
        """
        self.doc.preamble.append(Command('title', 'GitLab part of '+group.upper()+' PDL'))

        self.doc.preamble.append(Command('date', NoEscape(r'\today')))
        self.doc.preamble.append(Command('author',
                                         str(group+"'s developers")))   # author = developers
        self.doc.append(NoEscape(r'\maketitle'))
        self.doc.append(NoEscape(r'\setlength\parindent{0pt}'))         # noindent for all doc
        self.doc.add_color(name='lightgray', model="gray", description="0.80")    # get colors
        self.doc.append(NoEscape(r'\newpage'))
        self.doc.append(NoEscape(r'\tableofcontents'))
        self.doc.append(NoEscape(r'\newpage'))


    def introduction(self):
        """
        page de texte
        """
        intro_file = "./PDL_milestone.md"
        if not os.path.isfile(intro_file):
            self.doc.append(Command(r'textcolor{red}', "There is no introduction, sorry."))
            self.doc.append(NoEscape(r'\newline'))
            return

        with open(intro_file, "r", encoding='UTF-8') as file_in:
            lines = file_in.readlines()

        with self.doc.create(Section("Introduction")):
            for line in lines:
                myline0 = str(line.strip())
                # myline1 = myline0.replace("#", "\\texttt{\detokenize{#}}")
                myline1 = myline0.replace("#", "\\#")
                myline2 = myline1.replace("_", "\\_")
                self.doc.append(NoEscape(str(myline2)))

        self.doc.append(NoEscape(r'\newpage'))
        return


    def section_entete(self, milestone):
        """
        entete d'une section
        """
        with self.doc.create(Section(milestone.title+r' ('+ milestone.state+")")):

            # description
            description = milestone.description
            lines = description.splitlines(True)
            if not lines:
                self.doc.append(Command(r'textcolor{red}', "This milestone does not"+
                                        " have a description."))
                self.doc.append(NoEscape(r'\newline'))
                return

            for line in lines:
                myline = line.rstrip()
                myline = myline.lstrip()
                if myline != '\r\n':
                    self.doc.append(myline)
                    self.doc.append(NoEscape(r'\newline'))

            self.doc.append(NoEscape(r'\\[1.0cm]'))

        return


#
#  ISSUES
#
    def check_issues(self, milestone):
        """
        check if there is issue in this milestone
        """
        # num of issues for a milestone
        issues = milestone.issues(lazy=True)
        if not issues:
            self.doc.append(Command(r'textcolor{red}', "There is no Issues in this milestone."))
            self.doc.append(NoEscape(r'\newline'))
            return False
        return True

    def recap_issues(self, tab, milestone):
        """
        recap somes issues title from tab iid
        """
        issues = milestone.issues(lazy=True)
        for issue in issues:
            if issue.iid in tab:
                self.doc.append("#"+str(issue.iid)+" -- "+str(issue.title))
                self.doc.append(NoEscape(r'\newline'))

        self.doc.append(NoEscape(r'\newline'))


    def tab_issues_closed(self, milestone, projects):
        """
        tableau in section for all issues closed during last month
        """
        # num of issues for a milestone
        issues = milestone.issues(lazy=True)

        # si aucune issue n est fermee au moins n-1, on fait rien
        icount_last_month = 0
        for issue in issues:
            # if closed last month
            if ((issue.state == "closed") and (self.last_month == issue.closed_at[:7])):
                icount_last_month = icount_last_month + 1

        if not icount_last_month:
            self.doc.append(Command(r'textcolor{red}', "There is no Issues closed last"+
                                    " month for this milestone."))
            self.doc.append(NoEscape(r'\newline'))
            return

        self.doc.append(Command(r'textcolor{blue}', "Number Issues closed last month: "+
                                str(icount_last_month)))
        self.doc.append(NoEscape(r'\newline'))


        # table on issues for a milestone
        tab_issues_id = []
        with self.doc.create(Tabular('llcccl')) as table:
            self.entete_table_issues(table)

            for issue in milestone.issues():

                # nom projet
                project_name = ""
                for project in projects:
                    if project.id == issue.project_id:
                        project_name = project.name
                        break

                # assignation si il y a
                assignation = self.get_assignation_obj(issue)

                # label si il y a
                labels = self.get_labels_names(issue)

                # ligne du tableau
                if ((issue.state == "closed") and (self.last_month == issue.closed_at[:7])):

                    # si fermee au moins n-1
                    tab_issues_id.append(issue.iid)
                    table.add_row("#"+str(issue.iid),
                                  str(project_name),
                                  issue.state,
                                  assignation,
                                  issue.created_at[0:10],
                                  labels)

            table.add_hline()
        self.doc.append(NoEscape(r'\\[0.5cm]'))

        if tab_issues_id:
            self.recap_issues(tab_issues_id, milestone)
        return


    def tab_issues_opened_attr(self, milestone, projects):
        """
        tableau in section for all issues opened and attributed
        """
        # num of issues for a milestone
        issues = milestone.issues(lazy=True)


        # si aucune issue n est ouverte et attribuee, on fait rien
        tab_iss_ass = []
        for issue in issues:
            if ((issue.state == "opened") and (len(issue.assignees) != 0)):  # si ouverte
                tab_iss_ass.append(issue.iid)


        if not tab_iss_ass:
            self.doc.append(Command(r'textcolor{red}', "There is no Issues opened and"+
                                    " attributed for this milestone."))
            self.doc.append(NoEscape(r'\newline'))
            return

        self.doc.append(Command(r'textcolor{blue}', "Number Issues opened and attributed : "+
                                str(len(tab_iss_ass))))
        self.doc.append(NoEscape(r'\newline'))

        # table on issues for a milestone
        with self.doc.create(Tabular('llcccl')) as table:
            self.entete_table_issues(table)

            for issue in milestone.issues():

                # nom projet
                project_name = ""
                for project in projects:
                    if project.id == issue.project_id:
                        project_name = project.name
                        break

                # assignation si il y a
                if issue.iid in tab_iss_ass:
                    assignation = issue.assignee['name'].split()[1]  # nom de famille
                else:
                    assignation = "No one"

                # label si il y a
                labels = self.get_labels_names(issue)

                # ligne du tableau
                if ((issue.state == "opened") and (assignation != "No one")): # ouverte et assigne

                    table.add_row("#"+str(issue.iid),
                                  str(project_name),
                                  issue.state,
                                  assignation,
                                  issue.created_at[0:10],
                                  labels)

            table.add_hline()
        self.doc.append(NoEscape(r'\\[0.5cm]'))

        if tab_iss_ass:
            self.recap_issues(tab_iss_ass, milestone)
        return


    def tab_issues_opened_nonattr(self, milestone, projects):
        """
        tableau in section for all issues opened and unattributed
        """
        # num of issues for a milestone
        issues = milestone.issues(lazy=True)


        # si aucune issue n est ouverte et attribuee, on fait rien
        tab_iss_ass = []
        for issue in issues:
            if issue.state == "opened":  # si ouverte
                if not issue.assignees:
                    tab_iss_ass.append(issue.iid)


        if not tab_iss_ass:
            self.doc.append(Command(r'textcolor{red}', "There is no Issues opened and"+
                                    " unattributed for this milestone."))
            self.doc.append(NoEscape(r'\newline'))
            return

        self.doc.append(Command(r'textcolor{blue}', "Number Issues opened and unattributed : "+
                                str(len(tab_iss_ass))))
        self.doc.append(NoEscape(r'\newline'))

        # table on issues for a milestone
        with self.doc.create(Tabular('llcccl')) as table:
            self.entete_table_issues(table)

            for issue in milestone.issues():

                if issue.state != "opened":
                    break

                # nom projet
                project_name = ""
                for project in projects:
                    if project.id == issue.project_id:
                        project_name = project.name
                        break

                # assignation si il y a
                if issue.iid in tab_iss_ass:
                    assignation = "No one"
                else:
                    assignation = issue.assignee['name'].split()[1]  # nom de famille


                # label si il y a
                labels = self.get_labels_names(issue)

                # ligne du tableau
                if ((issue.state == "opened") and (assignation == "No one")):

                    # ouverte et non assigne
                    table.add_row("#"+str(issue.iid),
                                  str(project_name),
                                  issue.state,
                                  assignation,
                                  issue.created_at[0:10],
                                  labels)

            table.add_hline()
        self.doc.append(NoEscape(r'\\[0.5cm]'))

        if tab_iss_ass:
            self.recap_issues(tab_iss_ass, milestone)
        return



#
#  MERGE REQUEST
#


    def recap_mrs(self, tab, milestone):
        """
        recap in section for all mr
        """
        mrs = milestone.merge_requests(lazy=True)
        for merge_request in mrs:
            if merge_request.iid in tab:
                self.doc.append("!"+str(merge_request.iid)+" -- "+str(merge_request.title))
                self.doc.append(NoEscape(r'\newline'))

        self.doc.append(NoEscape(r'\newline'))



    def section_tab_mr(self, milestone):
        """
        tableau in section for all mr
        """
        # number of mr
        mrs = milestone.merge_requests(lazy=True)
        if not mrs:
            self.doc.append(Command(r'textcolor{red}', "There is no Merge Requests"+
                                    " in this milestone."))
            self.doc.append(NoEscape(r'\newline'))
            return

        # si aucune mr n est creee au moins n-1, on fait rien
        icount_last_month = sum(1 if self.last_month == merge_request.created_at[:7]
                                else 0 for merge_request in mrs)

        if icount_last_month == 0:
            self.doc.append(Command(r'textcolor{red}', "There is no Merge Request"+
                                    " created last month for this milestone."))
            self.doc.append(NoEscape(r'\newline'))
            return

        self.doc.append(Command(r'textcolor{blue}', "Number Merge Requests : "+str(len(mrs))))
        self.doc.append(NoEscape(r'\newline'))

        tab_mrs_id = []
        with self.doc.create(Tabular('lcccl')) as table:
            table.add_hline()
            table.add_row(("M-R", "State", "Assignee", "Date creation", "Label"))
            table.add_hline()
            for merge_request in mrs:

                # assignation si il y a
                assignation = self.get_assignation_obj(merge_request)

                # label si il y a
                labels = self.get_labels_names(merge_request)

                # ligne du tableau
                if self.last_month == merge_request.created_at[:7]:

                    tab_mrs_id.append(merge_request.iid)
                    table.add_row("!"+str(merge_request.iid),
                                  merge_request.state,
                                  assignation,
                                  merge_request.created_at[:10],
                                  labels)

            table.add_hline()
        self.doc.append(NoEscape(r'\\[0.5cm]'))

        if not tab_mrs_id:
            self.recap_mrs(tab_mrs_id, milestone)
        return
#
#   EXTRA
#
    def extra_section(self, issues, projects):
        """
        issues opened without milestone
        """
        tab_issue_nude = []
        for issue in issues:
            if issue.state == "opened":
                if isinstance(issue.milestone, type(None)):
                    tab_issue_nude.append(issue.id)


        if not tab_issue_nude:
            return

        with self.doc.create(Section("Issues opened with no milestone association")):
            self.doc.append(Command(r'textcolor{blue}', "Number Issues opened without milestone : "+
                                    str(len(tab_issue_nude))))
            self.doc.append(NoEscape(r'\newline'))


        # table on issues for a milestone
        with self.doc.create(Tabular('llcccl')) as table:
            self.entete_table_issues(table)

            for issue in issues:

                # nom projet
                project_name = ""
                for project in projects:
                    if project.id == issue.project_id:
                        project_name = project.name
                        break

                # assignation si il y a
                assignation = self.get_assignation_obj(issue)

                # label si il y a
                labels = self.get_labels_names(issue)

                # ligne du tableau
                if issue.id in tab_issue_nude:

                    table.add_row("#"+str(issue.iid),
                                  str(project_name),
                                  issue.state,
                                  assignation,
                                  issue.created_at[0:10],
                                  labels)

            table.add_hline()
        self.doc.append(NoEscape(r'\\[0.5cm]'))

        for issue in issues:
            if issue.id in tab_issue_nude:
                self.doc.append("#"+str(issue.iid)+" -- "+str(issue.title))
                self.doc.append(NoEscape(r'\newline'))


        self.doc.append(NoEscape(r'\newline'))
        return


#
#   GLOBAL FUNCTION
#
    def entete_table_issues(self, table):
        """
        debut d un tableau de issue
        """
        table.add_hline()
        table.add_row(("Issue",
                       "Project",
                       "State",
                       "Assignee",
                       "Date creation",
                       "Label"))
        table.add_hline()

    def get_labels_names(self, obj):
        """
        renvoie les labels d'une issue
        ou d'une MR en 1 str
        """
        labels = ""
        if not obj.labels:
            labels = "/"
        else:
            labs = obj.labels
            labs1 = str(labs)
            labels1 = labs1.replace("'", "")
            labels2 = labels1.replace("[", "")
            labels = labels2.replace("]", "")

        return labels

    def get_assignation_obj(self, obj):
        """
        find a assignee of an issue/merge request
        otherwise No one
        """
        if isinstance(obj.assignee, type(None)):
            assignation = "No one"
        else:
            assignation = obj.assignee['name'].split()[1]  # nom de famille

        return assignation



    def vspace(self, x_value):
        """
        espacement vertical
        """
        self.doc.append(NoEscape(r'\vspace{'+str(x_value)+'cm}'))


    def finish(self):
        """
        write pdf
        """
        self.doc.generate_pdf(self.fichier, clean_tex=True)
        self.doc.generate_pdf(self.fichier, clean_tex=True)
