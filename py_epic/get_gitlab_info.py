#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
module
Ce module contient l'implémentation de la class Getgitlabinfo
"""
import sys
import operator
import gitlab

class Getgitlabinfo():
    """
    Classe de definition des fonctions
    communiquant avec l api de gitlab pour
    rechercher des infos
    """

    def __init__(self, g_id, url, token):
        self.url = url
        self.token = token
        self.group_id = g_id

        # get connexion
        if not url:
            print("ERROR, url or token empty values", file=sys.stderr)
            sys.exit(1)

        try:
            if not token:
                gl_connect = gitlab.Gitlab(url=url)
            else:
                gl_connect = gitlab.Gitlab(url=url, private_token=token)
        except IOError as error:
            print("ERROR, can not connect to gitlab with this url/token "+error, file=sys.stderr)
            sys.exit(1)

        self.gitlab = gl_connect

        # get group
        try:
            self.group = self.gitlab.groups.get(int(self.group_id))
        except IOError as error:
            print(f"ERROR,{error}", file=sys.stderr)
            print(f"ERROR, The group with id {self.group_id} is not found", file=sys.stderr)
            sys.exit(1)

        self.group = self.gitlab.groups.get(self.group_id)
        self.projects = self.group.projects.list()


    def milestones(self):
        """
        Retourne la liste de toutes les milestones du projet
        triées alphabétiquement selon leur titre
        """
        all_milestones = []

        if not self.token:
            print('You have to put a token', file=sys.stderr)
            print('See https://gitlab.com/gitlab-org/gitlab-foss/-/issues/64269', file=sys.stderr)

        try:
            self.group.milestones.list(get_all=True)
        except IOError as error:
            print(f"ERROR,{error}", file=sys.stderr)
            print(f"ERROR,{error} Can not get milestones list.", file=sys.stderr)
            sys.exit(1)

        all_milestones = self.group.milestones.list(all=True)

        return sorted(all_milestones, key=operator.attrgetter("title"))

    def epics(self):
        """
        Communication entre gitlab et les epics du group
        """
        all_epics = []

        if not self.token:
            print('You have to put a token', file=sys.stderr)
            print('See https://gitlab.com/gitlab-org/gitlab-foss/-/issues/64269', file=sys.stderr)

        try:
            self.group.epics.list(get_all=True)
        except IOError as error:
            print(f"ERROR,{error}", file=sys.stderr)
            print(f"ERROR,{error} Can not get epics list.", file=sys.stderr)
            sys.exit(1)

        all_epics = self.group.epics.list(all=True)

        return sorted(all_epics, key=operator.attrgetter("title"))

    def issues(self):
        """
        Retourne la liste des issues du projet
        """
        return self.group.issues.list(all=True)

    def merge_requests(self):
        """
        Retourne la liste de toutes les merge requests du projet
        """
        return self.group.merge_requests(all=True)
