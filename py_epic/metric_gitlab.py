#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Main module python to collect GitLab informations
and write pdf file
"""
import sys
import signal
import argparse

import get_gitlab_info
import tex_gitlab_info


def write_doc_by_epics(group, url, token):
    """
    write pdf file on epics order informations
    """
    # on ouvre la connection
    git = get_gitlab_info.Getgitlabinfo(group, url, token)
    group = git.group.name
    projects = git.projects

    # infos
    issues = git.issues()
    epics = git.epics()

    # pylatex
    tex_file = tex_gitlab_info.Texgitlabinfo(group)

    # entete
    tex_file.entete(group)

    # intro
    tex_file.introduction()


    # main loop
    for epic in epics:
        tex_file.section_entete(epic)

        # issues
        if tex_file.check_issues(epic):
            tex_file.tab_issues_closed(epic, projects)         # closed during month -1
            tex_file.tab_issues_opened_attr(epic, projects)    # opened attributed
            tex_file.tab_issues_opened_nonattr(epic, projects) # opened unattributed

        tex_file.vspace(2.0)

    # issues opened without epic and other irregular issue
    tex_file.irregular_section(issues, projects, epics)

    # generate pdf
    tex_file.finish()


    print("1. Get info from group =", group)
    print("\t projects            =", len(projects))
    print("\t issues              =", len(issues))
    print("2. Pdf file is generated.")
    print("\t file                =", tex_file.fichier)


def usage():
    """
    Affiche l aide
    """
    print("run this script on python3 with arg : ")
    print(" ")
    print("  First put       <gitlab group id>   no default")
    print("  Second put      <personal token>    no default.")
    print(" ")


def signal_handler():
    """
    Pour interrompre le process lorsque l'on saisit "Ctrl+C"
    """
    print('Vous avez arrete le programme.')
    sys.exit(0)


if __name__ == "__main__":

    # ctrl + c
    # --------
    signal.signal(signal.SIGINT, signal_handler)

    # arguments
    # ---------
    PARSER_CONSTR = argparse.ArgumentParser(prog="Metric on GitLab",
                                            description="Scripting to get gitlab"+
                                            " epic on group info and write pdf")
    PARSER_CONSTR.add_argument("group", help="Identifiant du groupe", type=int)
    PARSER_CONSTR.add_argument("token", help="Token", type=str)

    ARGS_CONSTR = PARSER_CONSTR.parse_args()

    URL = "https://gitlab.com"
    GROUP = ARGS_CONSTR.group
    TOKEN = ARGS_CONSTR.token


    # write doc order by epics
    # ------------------------
    write_doc_by_epics(GROUP, URL, TOKEN)
