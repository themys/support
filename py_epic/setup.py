"""
module python only setuptools
"""
from setuptools import setup


with open('./requirements.txt', encoding='UTF-8') as f:
    REQUIRED_MOD = f.read().splitlines()

setup(
    install_requires=REQUIRED_MOD,
    name='metric_gitlab',
    version='0.0.1',
    author='racinej'
)
