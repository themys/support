Ce document regroupe les _milestone_ de la plate-forme Themys traités courant le mois précédent.

Grossiérement, les développements de priorité élevés attendus par les utilisateurs à cibler en priorité sont :
- la finalisation du dév. sur le filtre de construction d'interfaces (D005, Themys, #3),
- le lancement en mode batch (D001, Support, #29),
- la prise en compte du temps restant en batch (D001, Support, #31),

et plus généralement :
- l'extension des tests de la CI sur la plate-forme Themys afin de détecter au plus tôt toute régression en commençant par celle de ParaView (G003),
- l'extension des scripts d'installation et de lancement afin d'accélérer la mise à disposition de nouvelles versions aux utilisateurs d'une partie de la plate-forme Themys (D001).
